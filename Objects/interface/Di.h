#pragma once

#include <cmath>

#include <algorithm>
#include <iostream>
#include <type_traits>

#include "Core/Objects/interface/PhysicsObject.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Lepton.h"

#include "Core/Uncertainties/interface/Variation.h"

#include "Math/VectorUtil.h"

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Generic template class to store of a system of 2 objects and facilitate the
/// handling of the variations as well as the calculation of frequent variables.
///
/// `Obj1` and `Obj2` are expected to be either derivated from PhysicsObjects or
/// other specialisations of `Di<>` itself. Such nesting allows for complex
/// objects such as Z+jets or ttbar systems.
///
/// The components of the system are not stored directly but are pointers to
/// existing objects to avoid redundancy / inconsistencies and keep the object
/// light.
///
/// To obtain variations of the composite system, an instance of
/// `DAS::Uncertainties::Variation` may be given as argument of all relevant
/// methods. This object is passed over to the components of the system.
template<typename Obj1, typename Obj2> struct Di : public std::pair<Obj1*, Obj2*>,
                                                   public AbstractPhysicsObject {

    using base = std::pair<Obj1*, Obj2*>;

    using std::pair<Obj1*, Obj2*>::pair;   //!< Import constructors from `std::pair`
    using std::pair<Obj1*, Obj2*>::first,  //!< First particle in the pair (field imported from `std::pair`)
          std::pair<Obj1*, Obj2*>::second; //!< Second particle in the pair (field imported from `std::pair`)

public:
    explicit operator bool () const { return first && second; }

    inline void clear () { first = nullptr; second = nullptr; }

    inline FourVector CorrP4 (const Uncertainties::Variation& v = Uncertainties::nominal) const override
                        { return first->CorrP4(v) + second->CorrP4(v); }
    inline float      CorrPt (const Uncertainties::Variation& v = Uncertainties::nominal) const override
                        { return CorrP4(v).Pt(); }

    inline float Rapidity (const Uncertainties::Variation& v = Uncertainties::nominal) const
                        { return CorrP4(v).Rapidity(); }
    inline float AbsRap   (const Uncertainties::Variation& v = Uncertainties::nominal) const
                        { return std::abs(Rapidity(v)); }

    inline float DeltaPhi (const Uncertainties::Variation& v = Uncertainties::nominal) const
                        { return ROOT::Math::VectorUtil::DeltaPhi(first->CorrP4(v), second->CorrP4(v)); }
    inline float DeltaR   (const Uncertainties::Variation& v = Uncertainties::nominal) const
                        { return ROOT::Math::VectorUtil::DeltaR  (first->CorrP4(v), second->CorrP4(v)); }

    inline float Yboost (const Uncertainties::Variation& v = Uncertainties::nominal) const
                        { return (first->Rapidity(v) + second->Rapidity(v))/2; }
    inline float Ystar  (const Uncertainties::Variation& v = Uncertainties::nominal) const
                        { return (first->Rapidity(v) - second->Rapidity(v))/2; }

    inline float Ymax (const Uncertainties::Variation& v = Uncertainties::nominal) const
                        { return std::max(first->AbsRap(), second->AbsRap()); }
    inline float HT   (const Uncertainties::Variation& v = Uncertainties::nominal) const
                        { return (first->CorrPt(v) + second->CorrPt(v))/2; }

    inline double Weight (const Uncertainties::Variation& v = Uncertainties::nominal) const override
                        { return first->Weight(v) * second->Weight(v); }
};

using GenDijet = Di<GenJet,GenJet>;
using GenDimuon = Di<GenMuon,GenMuon>;
using GenZJet = Di<GenDimuon,GenJet>;
using RecDijet = Di<RecJet,RecJet>;
using RecDimuon = Di<RecMuon,RecMuon>;
using RecZJet = Di<RecDimuon,RecJet>;

} // end of DAS namespace

////////////////////////////////////////////////////////////////////////////////
/// Generic overload of `operator+` to define composite objects as follows:
/// ~~~cpp
/// RecDimuon recdimuon = genmuons.at(0) + genmuons.at(1);
/// RecZJet reczjet = recdimuon + genjets.at(0);
/// ~~~
template<typename Obj1, typename Obj2,
    class = typename std::enable_if_t<std::is_base_of_v<DAS::AbstractPhysicsObject, Obj1>>,
    class = typename std::enable_if_t<std::is_base_of_v<DAS::AbstractPhysicsObject, Obj2>>>
auto operator+ (Obj1& o1, Obj2& o2)
{
    return DAS::Di<Obj1,Obj2>{&o1,&o2};
}

template<typename Obj1, typename Obj2>
std::ostream& operator<< (std::ostream& s, const DAS::Di<Obj1,Obj2>& di)
{
    if (di)
        return s << '[' << *di.first << ", " << *di.second << "] = " << di.CorrP4();
    else
        return s << "\x1B[33minvalid system\x1B[0m";
}

#if defined(__ROOTCLING__)
#pragma link C++ class DAS::GenDijet::base +;
#pragma link C++ class DAS::GenDimuon::base +;
#pragma link C++ class DAS::GenZJet::base +;
#pragma link C++ class DAS::RecDijet::base +;
#pragma link C++ class DAS::RecDimuon::base +;
#pragma link C++ class DAS::RecZJet::base +;

#pragma link C++ class DAS::GenDijet +;
#pragma link C++ class DAS::GenDimuon +;
#pragma link C++ class DAS::GenZJet +;
#pragma link C++ class DAS::RecDijet +;
#pragma link C++ class DAS::RecDimuon +;
#pragma link C++ class DAS::RecZJet +;
#endif
