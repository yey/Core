#ifndef DOXYGEN_SHOULD_SKIP_THIS
#include "Core/Objects/interface/Weight.h"

#define BOOST_TEST_MODULE Weight
#include <boost/test/included/unit_test.hpp>

using namespace DAS;
using namespace std;

BOOST_AUTO_TEST_CASE( weight )
{
    Weight weight;
    BOOST_TEST(weight == 1);
    BOOST_TEST(weight*2 == 2);

    weight = 3;
    BOOST_TEST(weight == 3);
}

#endif
