#ifndef DOXYGEN_SHOULD_SKIP_THIS
#include "Core/Objects/interface/PhysicsObject.h"

#define BOOST_TEST_MODULE PhysicsObject
#include <boost/test/included/unit_test.hpp>

#include <iostream>

using namespace DAS;
using namespace std;

struct PhysicsObjectDummy : public PhysicsObject {
    static inline const char * const ScaleVar = "DummyScales",
                             * const WeightVar = "DummyWgts";
    PhysicsObjectDummy () = default;
    ~PhysicsObjectDummy () = default;
    std::string_view scale_group () const override { return ScaleVar; }
    std::string_view weight_group () const override { return WeightVar; }
};

BOOST_AUTO_TEST_CASE( scale )
{
    PhysicsObjectDummy obj;
    BOOST_TEST( obj.scales.size() == 1 );
    BOOST_TEST( obj.scales.front() == 1. );

    obj.p4 = {1,1,1,1};
    cout << obj << endl;

    obj.scales.front() = 2;
    BOOST_TEST( obj.scales.front() == 2. );
    cout << obj << endl;

    FourVector corr = obj.CorrP4();
    BOOST_TEST( obj.p4. Pt()*2 == corr.Pt() );
    BOOST_TEST( obj.CorrPt()   == corr.Pt() );

    BOOST_TEST( obj.Rapidity() == corr.Rapidity() );
    BOOST_TEST( obj.p4.Eta  () == corr.Eta     () );
}

BOOST_AUTO_TEST_CASE( weights )
{
    PhysicsObjectDummy obj;
    BOOST_TEST( obj.weights.size() == 1);
    BOOST_REQUIRE_NO_THROW( obj.weights[0] );
    BOOST_TEST( obj.weights[0] == 1);
}

#endif
