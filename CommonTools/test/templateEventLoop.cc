#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TString.h>
#include <TFile.h>
#include <TH1.h>

#include "Math/VectorUtil.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Template for function (TODO)
void EventLoop
           (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
            const fs::path& output, //!< output ROOT file (n-tuple)
            const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
            const int steering, //!< parameters obtained from explicit options 
            const DT::Slice slice = {1,0} //!< number and index of slice
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    unique_ptr<TChain> tIn = DT::GetChain(inputs);
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = unique_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    // TODO: fetch info from MetaInfo and/or fill it with new information

    vector<RecJet> * recJets = nullptr;
    tIn->SetBranchAddress("recJets", &recJets);
    vector<GenJet> * genJets = nullptr;
    if (isMC)
        tIn->SetBranchAddress("genJets", &genJets);
    // TODO: load any branch you may need

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        // TODO: modify the branches

        if ((steering & DT::fill) == DT::fill) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);
    tOut->Write();

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Template to loop on a n-tuple." /* TODO */,
                            DT::config | DT::split | DT::fill | DT::syst /* TODO */);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               /* TODO: add here any needed argument */;
        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::EventLoop(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
