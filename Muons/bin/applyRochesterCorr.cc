#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>
#include <array>
#include <optional>

#include "Core/Objects/interface/Format.h"
#include "Core/Objects/interface/Lepton.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"

#include "Core/JEC/interface/matching.h"

#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TRandom3.h>

#include "Math/VectorUtil.h"

#include "RoccoR.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

template<>
inline bool DAS::Matching<DAS::GenMuon,DAS::RecMuon>::InTail
                    (const DAS::GenMuon&,
                     const DAS::RecMuon&,
                     const float&)
{
    return false;
}

namespace DAS::Muon {

////////////////////////////////////////////////////////////////////////////////
/// [TWiki](https://twiki.cern.ch/twiki/bin/view/CMS/RochcorMuon)
///
///      set   nmembers  comment
/// Default  0     1     default, reference based on madgraph sample, with adhoc ewk (sw2eff and Z width) and Z pt (to match data) weights. 
/// Stat     1     100   pre-generated stat. replicas; 
/// Zpt      2     1     derived without reweighting reference pt to data. 
/// Ewk      3     1     derived without applying ad-hoc ewk weights 
/// deltaM   4     1     one representative set for alternative profile deltaM mass window 
/// Ewk2     5     1     reweight the reference from constant to s-dependent Z width 
/// -------------------------------------------------------------------------------------
/// For statistical replicas, std. dev. gives uncertainty.
/// For the rest, difference wrt the cental is assigned as syst. error.
/// Total uncertainty is calculated as a quadrature sum of all components. 
void applyRochesterCorr 
       (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
        const fs::path& output, //!< output ROOT file (n-tuple)
        const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
        const int steering, //!< parameters obtained from explicit options 
        const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    unique_ptr<TChain> tIn = DT::GetChain(inputs);
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = unique_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");

    vector<RecMuon> * recMuons = nullptr;
    tIn->SetBranchAddress("recMuons", &recMuons);
    vector<GenMuon> * genMuons = nullptr;
    if (isMC)
        tIn->SetBranchAddress("genMuons", &genMuons);

    auto tables = config.get<fs::path>("corrections.muons.rochester.tables");
    if (!fs::exists(tables))
        BOOST_THROW_EXCEPTION( fs::filesystem_error("Tables can't be found.",
                        tables, make_error_code(errc::no_such_file_or_directory)));
    metainfo.Set<fs::path>("corrections", "muons", "rochester", "tables", tables);
    RoccoR rc(tables.c_str());

    auto alternative = config.get<bool>("corrections.muons.rochester.alternative");
    if (alternative)
        BOOST_THROW_EXCEPTION( logic_error("Alternative nominal correction not yet implemented") ); // TODO
    metainfo.Set<bool>("corrections", "muons", "rochester", "alternative", alternative);

    const bool applySyst = (steering & DT::syst) == DT::syst;
    if (applySyst) {
        // TODO Zpt: rather provide as alternative? (issue #76)
        // TODO Ewk2: understand 3rd remark from MUON POG
        for (const string& syst: {"stat", "Zpt", "Ewk", "deltaM", "Ewk2"}) {
            metainfo.Set<string>("variations", RecMuon::WeightVar, syst + SysUp);
            metainfo.Set<string>("variations", RecMuon::WeightVar, syst + SysDown);
        }
    }

    optional<Matching<GenMuon,RecMuon>> matching = nullopt;
    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        if (isMC) matching.emplace(*genMuons);

        for (auto& recMuon: *recMuons) {

            float pt = recMuon.p4.Pt(),
                  eta = recMuon.p4.Eta(),
                  phi = recMuon.p4.Phi();
            int Q = recMuon.Q;
            cout << Q << '\t' << pt << '\t' << eta << '\t' << phi << endl;

            auto& nl = recMuon.nTkHits;

            // 1) get scale factor (lambda)
            function<double(int /* unc set */,int /* member */)> SF;
            if (isMC) {
                auto genMuon = matching->Closest(recMuon);

                if (genMuon.p4.Pt() < 7000)
                    SF = [&](int s, int m) {
                        return rc.kSpreadMC(Q, pt, eta, phi, genMuon.p4.Pt(), s, m);
                    };
                else {
                    static TRandom3 r(metainfo.Seed<897532>(slice));
                    const auto u = r.Rndm();
                    SF = [&](int s, int m) { return rc.kSmearMC(Q, pt, eta, phi, nl, u, s, m); };
                }
            }
            else 
                SF = [&](int s, int m) { return rc.kScaleDT(Q, pt, eta, phi, s, m); };

            // 2) store it in scale
            recMuon.scales.clear(); // TODO?
            auto nom = SF(0,0);
            recMuon.scales.push_back(nom);

            // 3) variations

            if (!applySyst) continue;

            static const int nvars = 5;
            recMuon.scales.reserve(1+2*nvars);
            auto push_back = [&recMuon,&nom](float unc) {
                recMuon.scales.push_back(unc);
                unc -= nom; unc *= -1; unc += nom;
                recMuon.scales.push_back(unc);
                // TODO: should we force up (down) in the first (second) place?
            };

            // 3a) first stat unc (from replicas)
            float sum1 = 0, sum2 = 0;
            for (int m = 0; m < 100; ++m) {
                float sf = SF(1, m);
                sum1 += sf;
                sum2 += sf*sf;
            }
            double unc = sqrt(sum1*sum1 - sum2) / 100;

            push_back(unc);

            // 3b) then the syst unc
            for (int s = 2; s <= nvars; ++s) {
                unc = SF(s,0);
                push_back(unc);
            }
        }
        if ((steering & DT::fill) == DT::fill) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);
    tOut->Write();

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of namespace DAS::Muon

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Apply muon energy scale factors.",
                            DT::config | DT::split | DT::fill | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("tables", "corrections.muons.rochester.tables", "location of "
                                        "muon Rochester correction in text format.")
               .arg<bool>("alternative", "corrections.muons.rochester.alternative",
                                         "flag to use alternative nominal correction");
        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Muon::applyRochesterCorr(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
