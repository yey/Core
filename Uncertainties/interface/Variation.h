#pragma once

#include <iostream>

#include <TString.h>

#include "Math/Vector4D.h"

#include "Core/Objects/interface/Weight.h"

namespace DAS {

typedef ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<float>> FourVector;

namespace Uncertainties {

////////////////////////////////////////////////////////////////////////////////
/// Generic structure to hold the relevant information to identify the variation
/// indices. This structure may be passed over to physics (simple or composite)
/// objects to vary (or not) the kinematics or the weight respectively.
struct Variation {

    const TString group; //!< e.g. event, recjet
    const TString name; //!< variation name (including "Up" or "Down")

    const std::size_t index; //!< index in the vector where the variation factor is stored
    const int bit; //!< correlation bit

    ////////////////////////////////////////////////////////////////////////////////
    /// Destructor
    ~Variation () = default;

    ////////////////////////////////////////////////////////////////////////////////
    /// Normal constructor
    Variation (const TString& group, const TString& name, size_t index = 0, int bit = 0) :
        group(group), name(name), index(index), bit(bit)
    { }

    ////////////////////////////////////////////////////////////////////////////////
    /// Retrieves a correction from a vector. Returns the value at index 0 in
    /// the vector, unless this systematic variation modifies the correction,
    /// in which case the modified value is returned.
    ///
    /// \param group An identifier for the group of variations, used when
    ///              retrieving active variations from the metainfo.
    /// \param corrections A vector of values from which to fetch corrections.
    template<class T>
    const T& getCorrection (const std::string_view& group,
                            const std::vector<T>& corrections) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Retrieves the weight of an object in the current variation. This function
    /// uses the static variable `WeightVar` in the object's class to determine
    /// which group it should use.
    ///
    /// \warning The class used to determine the group is determined at compile
    ///          time from the type of the variable passed as argument.
    template<class PhysicsObject>
    const Weight& getWeight (const PhysicsObject& object) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Retrieves the corrected four-momentum of an object in the current variation.
    /// This function uses the static variable `ScaleVar` in the object's class
    /// to determine which group it should use.
    ///
    /// \warning The class used to determine the group is determined at compile
    ///          time from the type of the variable passed as argument.
    template<class PhysicsObject>
    FourVector getCorrP4 (const PhysicsObject& object) const;
};

template<class T //!< expecting a type such as float, double, DAS::Weight
> inline const T& Variation::getCorrection (const std::string_view& group,
                                          const std::vector<T>& corrections) const
{
    if (index >= corrections.size() || group != this->group)
        return corrections.front();
    return corrections.at(index);
}

template<class PhysicsObject> // TODO: remove template once `RecPhoton` inherits from `PhysicsObject`
[[ deprecated ]]
inline const DAS::Weight& Variation::getWeight (const PhysicsObject& object) const
{
    return getCorrection(PhysicsObject::WeightVar, object.weights);
}

template<class PhysicsObject> // TODO: remove template once `RecPhoton` inherits from `PhysicsObject`
[[ deprecated ]]
inline FourVector Variation::getCorrP4 (const PhysicsObject& object) const
{
    return group == PhysicsObject::ScaleVar ? object.CorrP4(index) : object.CorrP4(0);
}

const Variation nominal {"", "nominal"};

inline std::ostream& operator<< (std::ostream& s, const DAS::Uncertainties::Variation& v)
{
    return s << v.group << ' ' << v.name << ' ' << v.index << ' ' << v.bit;
}

} // end of namespace Uncertainties

} // end of namespace DAS
