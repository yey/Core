#ifndef _matching_h_
#define _matching_h_

#include "Core/Objects/interface/Jet.h"
#include "Core/CommonTools/interface/Greta.h"

#include <cassert>
#include <iostream>
#include <vector>
#include <limits>
#include <iostream>

#include "Math/VectorUtil.h"

#include <TH3.h>

namespace DAS {

using ROOT::Math::VectorUtil::DeltaR;

static const float inf = std::numeric_limits<float>::infinity();
static const FourVector NoMatch(inf, inf, inf, inf); // for miss/fake entries

////////////////////////////////////////////////////////////////////////////////
/// Functor to perform the matching for the building of the RM
///
/// The matching is implemented as if, given a rec jet, one would look for the
/// best matching at gen level, but it can be inverted by swapping the definition
/// of the template (dirty hack, I admit).
///
/// The rec and gen jets can be swapped by playing with the template arguments.
///
/// According to a page on the [TWiki](https://twiki.cern.ch/twiki/bin/view/CMS/JetResolution),
/// the matching radius should be taken as half of the cone size radius...
template<typename GenJet, typename RecJet>
struct Matching {

    static TH3 * h_kL, * h_kR;
    static inline bool InTail (const GenJet&, const RecJet&, const float& rho);

    static float maxDR; //!< max angular distance (here for ak4: should be adapted for ak7 or ak8)

    std::vector<GenJet> mcands; //!< matching candidates

    const bool CoreOnly;
    const float rho;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    Matching
        (std::vector<GenJet> genJets, //!< list of generated jets (no double matching)
         const bool coreOnly = false,
         const float Rho = -1 // TODO: handle bad values
         ) :
            mcands(genJets), CoreOnly(coreOnly), rho(Rho)
    { }

    ////////////////////////////////////////////////////////////////////////////////
    /// Get the closest matched generated jet in the cone
    GenJet Closest (
            const RecJet& recjet) //!< reconstructed jet
    {
        GenJet matchGenJet;

        auto minDR = maxDR;
        auto BestMatch = mcands.end();
        for (auto it = mcands.begin(); it != mcands.end(); ++it) {

            auto thisDR = DeltaR(recjet.p4, it->p4);
            if (thisDR > minDR) continue;
            if (CoreOnly && InTail(*it, recjet, rho)) continue;
            minDR = thisDR;
            BestMatch = it;
        }

        if (BestMatch != mcands.end()) {
            matchGenJet = *BestMatch;
            mcands.erase(BestMatch);
        }
        else {
            matchGenJet.p4 = NoMatch;
        }

        return matchGenJet;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Get the highest-pt matched generated jet in the cone
    GenJet HighestPt (
            const RecJet& recjet) //!< reconstructed jet
    {
        GenJet matchJet;
        matchJet.p4 = NoMatch; // by default no match

        for (auto it = mcands.begin(); it != mcands.end(); ++it) {
            if (DeltaR(it->p4, recjet.p4) > maxDR) continue;
            if (CoreOnly && InTail(*it, recjet, rho)) continue;

            matchJet = *it;
            mcands.erase(it);
            break;
        }
        return matchJet;
    }
};

template<typename GenJet, typename RecJet>
float Matching<GenJet, RecJet>::maxDR = 0.2; // default value, corresponding to half of the R parameter of the clustering algorithm for AK4

template<typename GenJet, typename RecJet>
TH3 * Matching<GenJet, RecJet>::h_kL = nullptr;
template<typename GenJet, typename RecJet>
TH3 * Matching<GenJet, RecJet>::h_kR = nullptr;

} // end of namespace

// the two next functions could in principle be defined in the namespace, but for clarity 
// (i.e. to avoid any confusion between the template typenames and the actual classes),
// we use `DAS::` in front of the class names.

template<>
inline bool DAS::Matching<DAS::GenJet,DAS::RecJet>::InTail
                        (const DAS::GenJet& gen,
                         const DAS::RecJet& rec,
                         const float& rho)
{
    //cout << "-------\n";
    auto response = (rec.CorrPt() - gen.p4.Pt())/gen.p4.Pt();
    //cout << rec.CorrPt() << ' ' << rec.p4.Eta() << ' ' << rho << '\n';
    int i = DAS::Matching<DAS::RecJet,DAS::GenJet>::h_kL->GetXaxis()->FindBin(rec.CorrPt()),
        j = DAS::Matching<DAS::RecJet,DAS::GenJet>::h_kL->GetYaxis()->FindBin(rec.p4.Eta()),
        k = DAS::Matching<DAS::RecJet,DAS::GenJet>::h_kL->GetZaxis()->FindBin(rho);
    //cout << i << ' ' << j << ' ' << k << '\n';
    auto kL = DAS::Matching<DAS::RecJet,DAS::GenJet>::h_kL->GetBinContent(i,j,k),
         kR = DAS::Matching<DAS::RecJet,DAS::GenJet>::h_kR->GetBinContent(i,j,k);
    //cout << kL << ' ' << response << ' ' << kR << '\n';
    return response < kL || response > kR;
}

template<>
inline bool DAS::Matching<DAS::RecJet,DAS::GenJet>::InTail
                        (const DAS::RecJet& rec,
                         const DAS::GenJet& gen,
                         const float& rho)
{
    return DAS::Matching<DAS::GenJet,DAS::RecJet>::InTail(gen, rec, rho);
}

#endif
