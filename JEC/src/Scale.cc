#include "Core/JEC/interface/Scale.h"
#include <boost/exception/all.hpp>

#include <algorithm>

using namespace DAS;
using namespace edm;
using namespace std;
using namespace JetEnergy;

namespace fs = filesystem;

float Scale::GetL1Fast
    (float pt,  //!< transverse momentum
     float eta,  //!< pseudorapidity
     float area, //!< jet area
     float rho)  //!< soft activity
    const
{
    L1Fast->setJetPt(pt);
    L1Fast->setJetEta(eta);
    L1Fast->setJetA(area);
    L1Fast->setRho(rho);
    return L1Fast->getCorrection();
}

float Scale::GetL2Relative
    (float pt, //!< transverse momentum
     float eta) //!< pseudorapidity
    const
{
    L2Relative->setJetPt(pt);
    L2Relative->setJetEta(eta);
    return L2Relative->getCorrection();
}

float Scale::GetL3Absolute
    (float pt, //!< transverse momentum
     float eta) //!< pseudorapidity
    const
{
    L3Absolute->setJetPt(pt);
    L3Absolute->setJetEta(eta);
    return L3Absolute->getCorrection();
}

float Scale::GetL5Flavor 
    (float pt, //!< transverse momentum
     float eta, //!< pseudorapidity
     int partonFlavour) //!< parton flavour (DIFFERENT from hadron flavour!)
     const
{
    FactorizedJetCorrector * l5flavor = L5Flavor.at(abs(partonFlavour));
    assert(l5flavor != nullptr);

    l5flavor->setJetPt(pt);
    l5flavor->setJetEta(eta);
    return l5flavor->getCorrection();
}

float Scale::GetL2L3Residual (float pt, float eta) const
{
    L2L3Residual->setJetPt(pt);
    L2L3Residual->setJetEta(eta);
    return L2L3Residual->getCorrection();
}

float Scale::getJEC (float pt, float eta, float area, float rho, int pFl) const
{
    float L1corr = GetL1Fast(pt, eta, area, rho);
    pt *= L1corr;

    float L2corr = 1, L3corr = 1;
    if (wFl) {
        // this one replaces L2(L3)Relative
        L2corr = GetL5Flavor(pt, eta, pFl); // 
        pt *= L2corr;
    }
    else {
        // if L3 is trivial, this one is L2L3Relative (distinct from L2L4Residual, only for data)
        L2corr = Scale::GetL2Relative(pt, eta);
        pt *= L2corr;

        // this one is probably trivial, but just in case...
        L3corr = GetL3Absolute(pt, eta);
        pt *= L3corr;
    }

    float L2L3residual = GetL2L3Residual(pt, eta);
    //float L2L3residual = !isMC ? GetL2L3Residual(pt, eta) : 1.;

    return L1corr * L2corr * L3corr * L2L3residual;
}

float Scale::getJEC (const RecJet & jet, float  rho) const
{
    float pt = jet.p4.Pt();
    float eta = jet.p4.Eta();
    float area = jet.area;
    int pFl = jet.partonFlavour;
    return getJEC(pt, eta, area, rho, pFl);
}

pair<float,float> Scale::getTotalUnc (float pt, float eta) const
{
    // up
    uncProv->setJetEta(eta);
    uncProv->setJetPt(pt);
    float uncUp = uncProv->getUncertainty(true); 

    // down
    uncProv->setJetEta(eta);
    uncProv->setJetPt(pt);
    float uncDn = uncProv->getUncertainty(false); 

    return {uncUp, uncDn};
}


vector<float> Scale::getTotalUncVec (float pt, float eta) const
{   
    vector<float> vec(1, 1.);
    vec.reserve(3); // nominal 0, up 1, down 2

    // up
    uncProv->setJetEta(eta);
    uncProv->setJetPt(pt);
    float uncUp = uncProv->getUncertainty(true); 
    vec.push_back(1.+uncUp);

    // down
    uncProv->setJetEta(eta);
    uncProv->setJetPt(pt);
    float uncDn = uncProv->getUncertainty(false); 
    vec.push_back(1.-uncDn);

    return vec;
}

vector<float> Scale::getUncVec (float pt /* raw */, float eta) const
{
   vector<float> vec(1,1.); // include the central variation
   vec.reserve(2*uncSources.size()+1); // book 2 more slots per source
   for (auto uncSource: uncSources) {
       // note:
       // float JetCorrectionUncertainty::getUncertainty (bool fDirection)

       // up
       uncSource->setJetEta(eta);
       uncSource->setJetPt(pt);
       float uncUp = uncSource->getUncertainty(true); // TODO: L5?
       vec.push_back(1.+uncUp);

       // down
       uncSource->setJetEta(eta);
       uncSource->setJetPt(pt);
       float uncDn = uncSource->getUncertainty(false); // TODO: L5?
       vec.push_back(1.-uncDn);
   }
   return vec;
}

Scale::Scale (fs::path dir, int R) :
        // flags
        //isMC(dir.find("MC") != string::npos),
        wFl(false),
        // corrections
        L1Fast(nullptr), L2Relative(nullptr),
        L3Absolute(nullptr), L2L3Residual(nullptr),
        // uncertainties
        uncProv(nullptr)
{
    R = R < 6 ? 4 : 8;

    if (!fs::exists(dir))
        BOOST_THROW_EXCEPTION( fs::filesystem_error("Directory does not exists.",
                    dir, make_error_code(errc::no_such_file_or_directory)) );

    if (!fs::is_directory(dir))
        BOOST_THROW_EXCEPTION( fs::filesystem_error("Not a directory.",
                    dir, make_error_code(errc::not_a_directory)) );

    wFl = dir.string().find("MC") != string::npos &&
          dir.string().find("AK4") != string::npos &&
          dir.string().find("Flavor") != string::npos; // TODO?

    // examples:
    // Summer16_07Aug2017_V15_Flavor_Herwig_MC/Summer16_07Aug2017_V15_Flavor_Herwig_MC_b_L5Flavor_AK4PFchs.txt
    // Summer16_07Aug2017_V14/Summer16_07Aug2017BCD_V14_DATA/Summer16_07Aug2017BCD_V14_DATA_L1FastJet_AK4PFchs.txt
    auto tag = dir.filename().string();
    auto filename = [dir,tag,R](string type) -> fs::path {
        string filename = tag + '_' + type + "_AK" + to_string(R) + "PFchs.txt";
        fs::path p = dir / filename;
        if (!fs::exists(p))
            BOOST_THROW_EXCEPTION( fs::filesystem_error("File does not exists.",
                        p, make_error_code(errc::no_such_file_or_directory)) );
        return p;
    };

    auto initCorr = [&](string corrType) {
        fs::path n = filename(corrType);
        cout << "File with correction is " << n << endl;
        JetCorrectorParameters pars(n.c_str());
        return new FactorizedJetCorrector({pars});
    };

    L1Fast = initCorr("L1FastJet");
    L2Relative = initCorr("L2Relative");
    L3Absolute = initCorr("L3Absolute");
    //if (!isMC) L2L3Residual = initCorr("L2L3Residual");
    L2L3Residual = initCorr("L2L3Residual");

    if (wFl) {
        // gluon or undefined
        L5Flavor[0] = initCorr("g_L5Flavor");
        L5Flavor[21] = L5Flavor[0];

        // u and d have the same corrections
        L5Flavor[1] = initCorr("ud_L5Flavor");
        L5Flavor[2] = L5Flavor[1];

        // others are unique
        L5Flavor[3] = initCorr("s_L5Flavor");
        L5Flavor[4] = initCorr("c_L5Flavor");
        L5Flavor[5] = initCorr("b_L5Flavor");
    }

    fs::path p = filename("Uncertainty");
    uncProv = new JetCorrectionUncertainty(p.c_str());

    uncSources.reserve(uncs.size());
    for (auto unc: uncs) {
        fs::path p = filename("UncertaintySources");
        JetCorrectorParameters pars(p.c_str(), unc.Data());
        uncSources.push_back(new JetCorrectionUncertainty(pars));
    }
}

const vector<TString> Scale::uncs {
        //flat absolute scale uncertainties. Main uncertainties are combined photon, Z->ee (electron) and Z->mumu (tracking) reference scale (AbsoluteScale) and correction for FSR+ISR (MPFBias). (AbsoluteFlavMap is obsolete and always zero)
        "AbsoluteStat", "AbsoluteScale",  "AbsoluteMPFBias",
        //"AbsoluteFlavMap",

        //high pT extrapolation. Based on Pythia6 Z2/Herwig++2.3 differences in fragmentation and underlying event (FullSim). (not yet updated since Run I)
        "Fragmentation",

        //high pT extrapolation. Based on propagation of +/-3% variation in single particle response in ECAL to PF Jets (FastSim). (shape and magnitude from Run I)
        "SinglePionECAL",
        //high pT extrapolation. Based on propagation of +/-3% variation in single particle response in HCAL to PF Jets (FastSim). (shape from Run I, magnitude refit in Run II)
        "SinglePionHCAL",

        // jet flavor. Based on Pythia6 Z2/Herwig++2.3 differences in uds/c/b-quark and gluon responses. Default flavor uncertainty is FlavorQCD. There exist alternative flavor uncertainty sources for other flavor compositions: FlavorZJet (Z+jet events), FlavorPhotonJet (gamma+jet events), FlavorPureGluon (only gluons), FlavorPureQuark (only uds), FlavorPureCharm (only c) and FlavorPureBottom (only b). (not yet updated since Run I)
        "FlavorQCD",
        //"FlavorZJet", "FlavorPhotonJet", "FlavorPureGluon", "FlavorPureQuark", "FlavorPureCharm", "FlavorPureBottom",

        //JEC time dependence between BCD, EF, G and H. The L2L3Residual is now corrected per epoch, so !TimePtEta! uncertainty is taken as a difference between the luminosity-weighted average of corrections per epoch, and a single L2L3Residual correction derived on the full BCDEFGH sample. The uncertainties for individual epochs (_TimeRun[BCD][EF][G][H]) are calculated as a weighted difference to BCDEFGH such that their luminosity-weighted sum in quadrature equals TimePtEta.
        "TimePtEta",
        //"TimeRunBCD", "TimeRunEF", "TimeRunGH",

        //eta-dependence uncertainty from jet pT resolution (JER). Derived by varying JER scale factors up and down by quoted uncertainties. The JER uncertainties are assumed fully correlated for endcap within tracking (EC1), endcap outside tracking (EC2) and hadronic forward (HF).
        "RelativeJEREC1", "RelativeJEREC2", "RelativeJERHF",

        //half-difference between MPF method log-linear (default) and constant fits versus pT. The pT-dependence has increased considerably since Run I, but the reason is not yet understood, and leads to more ambiguity in the parameterization. Therefore these uncertainties may not be entirely reliable yet.
        "RelativePtBB", "RelativePtEC1", "RelativePtEC2", "RelativePtHF",

        //full difference between log-linear fits of MPF and pT balance methods. This difference was negligible in Run I, but is inexplicably large in Run II. The reason is not yet understood, and this uncertainty source may still underestimate the true uncertainty.
        "RelativeBal",

        // eta-dependent uncertainty due to difference between relative residuals observed with dijet, Z+jets and gamma+jets (New, added in JEC for "07Aug" ReReco)
        "RelativeSample",

        //eta-dependence uncertainty due to correction for initial and final state radiation, estimated from difference between MPF log-linear L2Res from Pythia8 and Herwig++, after each has been corrected for their own ISR+FSR correction.
        "RelativeFSR",

        //statistical uncertainty in determination of eta-dependence, calculated from the error matrix of the FSR correction fit vs eta or L2Res log-linear fit vs pT. Latter is only important in endcap (EC) and in HF, where the uncertainty is provided as correlated over these wider regions.
        "RelativeStatFSR", "RelativeStatEC", "RelativeStatHF",

        //estimating 5% uncertainty on the data/MC scale factor for offset correction, which roughly covers the variation versus mu seen in the Random Cone method applied on Zero Bias data and neutrino gun MC.
        "PileUpDataMC",

        //pile-up offset dependence on jet pT is estimated from matched MC with and without PU overlay. The uncertainty is taken as difference to Random Cone method (i.e. essentially difference of PU inside and outside of jets), propagated through L2 (BB,EC,HF) and L3 (Ref) data-driven methods. The component absorbed in L2 and L3 data-driven methods is provided as special optional PileUpMuZero source, which should be used instead of PileUpPt source for no pile-up (or almost zero pileup, Mu=Zero) data. The original MC truth and Random Cone difference is provided as PileUpEnvelope source for expert use.
        "PileUpPtRef", "PileUpPtBB", "PileUpPtEC1", "PileUpPtEC2", "PileUpPtHF",
        //"PileUpMuZero", "PileUpEnvelope",

        //Total Unertainties
        //"SubTotalPileUp",
        //"SubTotalRelative",
        //"SubTotalPt",
        //"SubTotalScale",
        //"SubTotalAbsolute",
        //"SubTotalMC",
        //"Total",
        //"TotalNoFlavor",
        //"TotalNoTime",
        //"TotalNoFlavorNoTime",

        /*
           [CorrelationGroupMPFInSitu]
           [CorrelationGroupIntercalibration]
           [CorrelationGroupbJES]
           [CorrelationGroupFlavor]
           [CorrelationGroupUncorrelated]
           */
};
